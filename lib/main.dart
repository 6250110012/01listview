import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://freesvg.org/img/1535058967.png'),
    NetworkImage('https://freesvg.org/img/1590064906sail-ship-silhouette-freesvg.org.png'),
    NetworkImage('https://freesvg.org/img/Anonymous_Bus_symbol_black.png'),
    NetworkImage('https://freesvg.org/img/molumen-Old-rover-car.png'),
    NetworkImage('https://freesvg.org/img/train1.png'),
    NetworkImage('https://freesvg.org/img/1502546043.png'),
    NetworkImage('https://freesvg.org/img/Metro-10.png'),
    NetworkImage('https://freesvg.org/img/34-Hotel-Icon-Near-Transit-Stop.png'),
    NetworkImage('https://freesvg.org/img/1298514237.png')
  ];


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('ลิสวิว'),
        ),
        body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(
                    backgroundImage: images[index],
                  ),
                  title: Text(
                    '${titles[index]}',
                    style: TextStyle(fontSize: 18),
                  ),
                  subtitle: Text(
                    'There are many passengers in vehicles',
                    style: TextStyle(fontSize: 15),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {
                    AlertDialog alert = AlertDialog(
                      title: Text('Welcome'),
                      // To display the title it is optional
                      content: Text('This is a ${titles[index]}'),
                      // Message which will be pop up on the screen
                      // Action widget which will provide the user to acknowledge the choice
                      actions: [
                        ElevatedButton(
                          // FlatButton widget is used to make a text to work like a button

                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          // function used to perform after pressing the button
                          child: Text('CANCEL'),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text('ACCEPT'),
                        ),
                      ],
                    );
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return alert;
                      },
                    );
                  },
                ),
                Divider(
                  thickness: 0.8,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
